#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0;

    int xnov = 1;
    int ynov = 1;

    int mx;
    int my;

    for ( ;; ) {

        getmaxyx ( ablak, my , mx );

        mvprintw ( y, x, "O" );

        refresh ();
        usleep ( 100000 );
        clear();

        x = x + xnov;
        y = y + ynov;

        for ( int i = x; i>=mx-1; i-- ) { // elerte-e a jobb oldalt?
            xnov = xnov * -1;
        }
        for ( int i = x; i<=0; i++ ) { // elerte-e a bal oldalt?
            xnov = xnov * -1;
        }
        for (int i = y; i<=0; i++ ) { // elerte-e a tetejet?
            ynov = ynov * -1;
        }
        for (int i = y; i>=my-1; i-- ) { // elerte-e a aljat?
            ynov = ynov * -1;
        }

    }

    return 0;
}
