import tensorflow as tf
#import=include

tf.compat.v1.enable_eager_execution(
    config=None, device_policy=None, execution_mode=None

    )
mnist = tf.keras.datasets.mnist
#objektum, ide tesszük az adatainkat 

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0
#betöltjük az adatainkat, egymásra helyezzük a rétegeket, igy az adat megfelel
#a várt modellnek

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10)
])

""" elkészítjük az adatmodellt, a beépített keras segítségével 
egy logit es logg-odds vektort fog visszaadni. 
logit= nem normalizált, kezdeti érték amit a modell generál
log-odd= valószínűségszámítás arányban kifejezve"""

predictions = model(x_train[:1]).numpy()
predictions
#neurális háló kimenete

tf.nn.softmax(predictions).numpy()
#softmax predikciókra, transzformáljuk osztályokká

loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

loss_fn(y_train[:1], predictions).numpy()

# megmutatja mekkora volt a különbség a predikció és output között, jellemzi
#a tévedés mértékét


model.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])

#minimalizáljuk a losst

model.fit(x_train, y_train, epochs=10)

#maga a tanitás inditása

model.evaluate(x_test,  y_test, verbose=2)
#kiértékelés a test adatbázisra

probability_model = tf.keras.Sequential([
  model,
  tf.keras.layers.Softmax()
])
#valószínűséget ad vissza

probability_model(x_test[:5])
