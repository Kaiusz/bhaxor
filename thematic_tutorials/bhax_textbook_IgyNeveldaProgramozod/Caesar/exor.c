#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define maxk 100
#define buffm 256

int main(int argc, char **argv)
{
    char kulcs[maxk];
    char buff[buffm];
    
    int kulcsi=0;
    int olvasottbajtok=0;
    
    int kulcs_meret = strlen (argv[1]);
    strncpy (kulcs, argv[1], maxk);
    
    while ((olvasottbajtok = read (0, (void*) buff, buffm )))
    {
        for (int i=0; i<olvasottbajtok; i++)
        {
            buff[i]=buff[i] ^ kulcs[kulcsi];
            kulcsi =(kulcsi +1) % kulcs_meret;
        }
        write(1, buff, olvasottbajtok);
    }
}
