1;
function P=solve(M)
  if M<2 
    P=0.0;
    return;
  end
  if M>365 
    P=1.0;
    return;
  end
  P=1.0-prod(1.0-(0:(M-1))/365);
end

M=sscanf(fgetl(stdin),"%d");
fprintf(stdout,"%.9f\n",solve(M));