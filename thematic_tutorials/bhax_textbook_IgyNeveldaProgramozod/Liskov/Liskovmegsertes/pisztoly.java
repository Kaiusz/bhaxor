package Liskovmegsertes;

interface H {
}

interface ifwater extends H {
	public void habitat();
}

interface ifnotwater extends H {
}

class tuna implements ifwater {
	public void habitat() {
		System.out.println("Tuna: water\n");
	}
}

class ig implements ifnotwater {
}

class pisztoly {

	public static void habitatH(ifwater elem) {
		elem.habitat();
	}

	public static void main(String[] args) {

		tuna t = new tuna();
		ig mudskipper = new ig();

		habitatH(t);
		// habitatH(mudskipper);

	}

}
