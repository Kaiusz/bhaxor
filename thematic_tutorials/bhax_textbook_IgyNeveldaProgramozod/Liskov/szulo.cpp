#include <iostream>
#include <stdio.h>

class Parent{
    public:
    void Pmessage(){
        std::cout << "Parent message\n";
    }
};

class Child : public Parent{
    public:
    void Cmessage(){
        std::cout << "Child message\n";
    }
};

int main(){
    Parent* p= new Parent();
    Parent* c= new Child();

    p->Pmessage();
    //c->Cmessage();
    //p-> Cmessage();
    c-> Pmessage();

    delete p;
    delete c;
};

