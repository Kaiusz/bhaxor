#include <iostream>
class fish
{
public:
    void habitat()
    {
        std::cout << "lives in water...\n";
    }
};
class tuna : public fish
{
};
class mudskipper : public fish
{
};

int main()
{
    tuna thetuna;
    mudskipper themudskipper;
    thetuna.habitat();
    themudskipper.habitat();
    return 0;
}