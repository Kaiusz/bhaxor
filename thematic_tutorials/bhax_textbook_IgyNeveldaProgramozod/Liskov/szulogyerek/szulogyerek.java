package szulogyerek;

class Parent{
public void Pmessage() {
	System.out.println("Szülő üzenete");
}
}

class Child extends Parent{
	public void Cmessage() {
		System.out.println("Gyerek üzenete");
	}
}



public class szulogyerek {

	public static void main(String[] args) {
		Parent p= new Parent();
		Parent c= new Child();
		
		p.Pmessage(); //szülő objektum ismeri a saját metódusát
		c.Pmessage(); //gyerek objektum ismeri a szülő metódusát
		//c.Cmessage(); gyerek objektum nem ismeri a saját metódusát szülő referencián keresztül
		//p.Cmessage();	szülő objektum nem ismeri a gyerek metódusát	

	}

}
