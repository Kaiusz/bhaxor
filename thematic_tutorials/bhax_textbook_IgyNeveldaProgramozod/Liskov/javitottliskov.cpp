#include <iostream>
class fish
{
};
class ifwater : public fish
{
public:
    void fly();
};
class ifnotwater : public fish
{
};
class tuna : public ifwater
{
public:
    void habitat()
    {
        std::cout << "lives in water...\n";
    }
};
class mudskipper : public ifnotwater
{
};

int main()
{
    tuna thetuna;
    mudskipper themudskipper;
    thetuna.habitat();
    //themudskipper.habitat();

    return 0;
}